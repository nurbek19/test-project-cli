#!/usr/bin/env node

const axios = require('axios');

const singer = process.argv.slice(2);

if (!singer.length) {

  console.log('Please enter the artist name!');

} else {
  const url = `https://api.deezer.com/search?q=${singer.join(' ')}`;

  axios.get(url, { headers: { Accept: 'application/json' } })
    .then(
      (response) => {
        if (!response.data.data.length) {
          console.log('No records found');
        } else {
          response.data.data.slice(0, 5).forEach((song) => {
            console.log(song.title + ' ' + song.link);
          });
        }
      },
      (error) => {
        console.log(error);
      }
    ).catch((error) => {
      console.log(error);
  });
}
